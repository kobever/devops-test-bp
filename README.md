# devops-test
Testing Microservice

The aplication can ben conteinerized using the Dockerfile

Build command used to create the image:

$ docker build -t my-image .
------------------------------------------
After the image is built can be start the container using:

$ docker run -d -p 80:80 --name node-api  my-image
--------------------------------------------------------

The CI/CD is executed by the bitbucket-pipelines.yml file with every commit and push the image to my personal repo https://hub.docker.com/repository/docker/kobever1/devops-test-bp


---------------------------------------------------------------------------------

The Terraform_files folder have the files needed to deploy the Infrastructure in AWS

