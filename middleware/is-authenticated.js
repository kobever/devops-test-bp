const jwt = require('jsonwebtoken');
const APIKEY = '2f5ae96c-b558-4c7b-a590-a501ae1c3f6c';

module.exports = (req, res, next) => {
    const apikey = req.get('X-Parse-REST-API-Key');
    const jtoken = req.get('X-JWT-KWY');
    console.log(apikey);
    //console.log(jtoken);

    if (apikey !== APIKEY) {
        return res.status(404).send('ERROR');
    }

   next();
};